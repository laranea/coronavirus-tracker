# project/api/coronavirus.py

from flask_restx import Namespace, Resource

from project.api.utils import (  # noqa isort:skip
    calculate_totals,
    get_data,
    get_province_data,
    get_summary,
    get_state_data,
    get_standard_model,
    get_summary_latest,
)

coronavirus_namespace = Namespace("coronavirus")

BASE_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/"  # noqa: E501
CONFIRMED_CASES_URL = f"{BASE_URL}/time_series_covid19_confirmed_global.csv"
DEATH_DATASET_URL = f"{BASE_URL}/time_series_covid19_deaths_global.csv"
RECOVERED_DATASET_URL = f"{BASE_URL}/time_series_covid19_recovered_global.csv"
US_STATES_DATASET_URL = "https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv"  # noqa: E501


class Coronavirus(Resource):
    def get(self):
        headers, raw_data, last_updated = get_data(CONFIRMED_CASES_URL)
        country_data, country_names = calculate_totals(raw_data)

        payload = {
            "country_data": country_data,
            "last_updated": last_updated,
        }
        return payload


class CoronavirusSummaryCases(Resource):
    def get(self):
        headers, raw_data, last_updated = get_data(CONFIRMED_CASES_URL)
        country_data, country_names = calculate_totals(raw_data)
        summary = get_summary(country_data, headers, "cases")

        payload = {
            "summary": summary,
            "countries": country_names,
            "last_updated": last_updated,
        }
        return payload


class CoronavirusSummaryDeaths(Resource):
    def get(self):
        headers, raw_data, last_updated = get_data(DEATH_DATASET_URL)
        country_data, country_names = calculate_totals(raw_data)
        death_data = get_summary(country_data, headers, "deaths")

        payload = {
            "summary": death_data,
            "countries": country_names,
            "last_updated": last_updated,
        }
        return payload


class CoronavirusCanadaProvincesSummaryDeaths(Resource):
    def get(self):
        # return get_province_state_data(False)
        headers, raw_data, last_updated = get_data(DEATH_DATASET_URL)
        province_data, province_keys = get_province_data(headers, raw_data, "deaths")

        payload = {
            "summary": province_data,
            "provinces": province_keys,
            "last_updated": last_updated,
        }

        return payload


class CoronavirusCanadaProvincesSummaryCases(Resource):
    def get(self):
        # return get_province_state_data(False)
        headers, raw_data, last_updated = get_data(CONFIRMED_CASES_URL)
        province_data, province_keys = get_province_data(headers, raw_data, "cases")

        payload = {
            "summary": province_data,
            "provinces": province_keys,
            "last_updated": last_updated,
        }

        return payload


class CoronavirusAmericanStatesSummaryCases(Resource):
    def get(self):
        headers, raw_data, last_updated = get_data(US_STATES_DATASET_URL)
        data = get_state_data(raw_data, False)
        model, states = get_standard_model(data)

        payload = {"summary": model, "states": states, "last_updated": last_updated}
        return payload


class CoronavirusAmericanStatesSummaryDeaths(Resource):
    def get(self):
        headers, raw_data, last_updated = get_data(US_STATES_DATASET_URL)
        data = get_state_data(raw_data, True)
        model, states = get_standard_model(data)

        payload = {"summary": model, "states": states, "last_updated": last_updated}
        return payload


class CoronavirusWorldWideSummary(Resource):
    def get(self):
        headers, raw_data_deaths, _ = get_data(DEATH_DATASET_URL)
        headers, raw_data_cases, _ = get_data(CONFIRMED_CASES_URL)
        headers, raw_data_recovered, last_updated = get_data(RECOVERED_DATASET_URL)

        total_cases = get_summary_latest(raw_data_cases)
        total_deaths = get_summary_latest(raw_data_deaths)
        total_recovered = get_summary_latest(raw_data_recovered)
        payload = {
            "worldwide_cases": f"{total_cases:,}",
            "worldwide_deaths": f"{total_deaths:,}",
            "worldwide_recovered": f"{total_recovered:,}",
            "last_updated": last_updated,
        }

        return payload


coronavirus_namespace.add_resource(Coronavirus, "/")
coronavirus_namespace.add_resource(CoronavirusSummaryCases, "/countries/cases")
coronavirus_namespace.add_resource(CoronavirusSummaryDeaths, "/countries/deaths")
coronavirus_namespace.add_resource(
    CoronavirusCanadaProvincesSummaryDeaths, "/provinces/deaths"
)
coronavirus_namespace.add_resource(
    CoronavirusCanadaProvincesSummaryCases, "/provinces/cases"
)
coronavirus_namespace.add_resource(
    CoronavirusAmericanStatesSummaryCases, "/states/cases"
)
coronavirus_namespace.add_resource(
    CoronavirusAmericanStatesSummaryDeaths, "/states/deaths"
)
coronavirus_namespace.add_resource(CoronavirusWorldWideSummary, "/summary")
