# build
FROM node:13.12.0-alpine as build-vue
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./services/client/package*.json ./
RUN npm install
COPY ./services/client .
RUN npm run build

# production
FROM nginx:stable-alpine as production
WORKDIR /app
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_ENV production
ENV APP_SETTINGS project.config.ProductionConfig
ARG SECRET_KEY
ENV SECRET_KEY $SECRET_KEY
ARG MONITAUR_CLIENT_SECRET
ENV MONITAUR_CLIENT_SECRET $MONITAUR_CLIENT_SECRET
RUN apk update && apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
COPY --from=build-vue /app/dist /usr/share/nginx/html
COPY ./services/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ./services/server/requirements.txt ./
RUN pip install -r requirements.txt
COPY ./services/server .
CMD gunicorn -b 0.0.0.0:5000 manage:app --daemon && \
      sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && \
      nginx -g 'daemon off;'
